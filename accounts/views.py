from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Customer, Order,Products,User,Tag
from .forms import OrderForm,CreateUserForm, CustomerForm
from django.forms import inlineformset_factory
from .filters import OrderFilter
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .decoretors import unauthenticated_user, allowed_users,admin_only
from django.contrib.auth.models import Group

# Create your views here.
def csrf_failure(request, reason=""):
    ctx = {'message': 'some custom messages'}
    print('redirecting............', )
    return redirect(request.path)
    return render(request,'accounts/csrf_view.html', ctx)

@unauthenticated_user
def registerPage(request):
    
    form= CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username=form.cleaned_data.get('username')
            
            messages.success(request,'Account was created for '+ username)
            
            return redirect('login')
    contex={'form':form}
    return render(request, 'accounts/register.html',contex)

@unauthenticated_user
def loginPage(request):
    

    if request.method =="POST":
        username =  request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username,password = password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            messages.info(request,"Error Credentials")

    contex={}
    return render(request, 'accounts/login.html',contex)
def logoutUser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
@admin_only
def home(request):
    customers = Customer.objects.all()
    orders = Order.objects.all()
    total_customers = customers.count()
    total_orders = orders.count()
    delivered = orders.filter(status ='Delivered').count()
    pending = orders.filter(status= 'Pending').count()
    
    context = {
        'orders': orders,
        'customers': customers,
        'total_customers':total_customers,
        'total_orders' : total_orders,
        'delivered': delivered,
        'pending': pending,
    }

    return render(request,'accounts/dashboard.html',context)


@login_required(login_url='login')
@allowed_users(['customer'])
def userPage(request):

    orders = request.user.customer.order_set.all()
    total_orders = orders.count()
    delivered = orders.filter(status ='Delivered').count()
    pending = orders.filter(status= 'Pending').count()
    
    context = {
        'orders': orders,
        'total_orders' : total_orders,
        'delivered': delivered,
        'pending': pending,
    }
    return render(request, 'accounts/user.html', context)

@login_required(login_url='login')
@allowed_users(['customer'])
def accountSettings(request):
    customer =  request.user.customer
    form  = CustomerForm(instance=customer)

    pro_pic=''
    if request.user.customer.profile_pic:
        pro_pic= request.user.customer.profile_pic.url

    print(pro_pic)
    if request.method == 'POST':
        form = CustomerForm(request.POST, request.FILES,instance= customer)
        if form.is_valid():
            form.save()

    context={'form':form,'pro_pic': pro_pic}
    return render(request,'accounts/account_settings.html',context)

@login_required(login_url='login')
@allowed_users(['admin'])
def products(request):
    products = Products.objects.all()
    return render(request,'accounts/products.html',{'products': products})



def createCustomer(request):
    form = CustomerForm()
    form2 = CreateUserForm()
    if request.method == 'POST':
        form2 = CreateUserForm(request.POST)
        form2.first_name = request.POST['name']
        print('form name : ',form2.first_name)
        if form2.is_valid():
            form2.save()
        return HttpResponse("Done")
    contex={'form':form,'form2':form2}

    return render(request,'accounts/customer_form.html', contex)



@login_required(login_url='login')
@allowed_users(['admin'])
def customer(request,id):
    customer = Customer.objects.get(id=id)
    orders = customer.order_set.all()
    orders_count =  orders.count()
    myFilter = OrderFilter(request.GET, queryset= orders)
    orders = myFilter.qs
    context = {
        'customer':customer, 
        'orders':orders, 
        'orders_count': orders_count,
        'myFilter': myFilter,
        }
    return render(request,'accounts/customer.html',context)


@login_required(login_url='login')
@allowed_users(['admin'])
def createOdred(request,id):
    OrderFormSet = inlineformset_factory(Customer,Order,fields=('product','status'),extra=5)
    customer = Customer.objects.get(id=id)
    formset = OrderFormSet(queryset=Order.objects.none(),instance =customer)

    if request.method == 'POST':
        # form = OrderForm(request.POST)
        formset = OrderFormSet(request.POST,instance = customer)
        if formset.is_valid():
            formset.save()
            return redirect('/') 
            

    context ={'form': formset}
    return render(request,'accounts/order_form.html',context)


@login_required(login_url='login')
@allowed_users(['admin'])
def updateOrder(request,id):

    order = Order.objects.get(id=id)
    form = OrderForm(instance=order)
    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        if form.is_valid:
            form.save()
            return redirect('/')
    context ={'form' :form}
    return render(request,'accounts/order_form.html',context)


@login_required(login_url='login')
@allowed_users(['admin'])
def deleteOrder(request,id):
    order =Order.objects.get(id=id)
    order.delete()
    return redirect('/')
    
